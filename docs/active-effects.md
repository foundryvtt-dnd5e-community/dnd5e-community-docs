# Active Effects

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

?> See [Kandashi's Active Effects](https://docs.google.com/document/d/1DuZaIFVq0YulDOvpahrfhZ6dK7LuclIRlGOtT0BIYEo) guide for a treasure trove of information.

## Configuration

- What can they affect? What can't they affect?
- How do I determine the property path to target?

## Usage

- Active Effects on Items get transferred to their owning actor?
- How does a GM apply an offensive effect?

## Examples

### Movement

```
data.attributes.movement.walk
                         fly
                         swim
                         burrow
                         climb
```

#### Multiply Speed by modifier

E.g. An Item or Spell which doubles/halves/etc. an Actor's speed.

| Attribute Key | Change Mode | Effect Value |
| -------- | -------- | -------- |
| `data.attributes.movement.walk`     | Multiply     | `2`     |

#### Add a different Speed

E.g. An Item or Spell which grants an Actor a flying or swimming speed.

| Attribute Key | Change Mode | Effect Value |
| -------- | -------- | -------- |
| `data.attributes.movement.fly`     | Override     | `30`     |

### Armor Class

```
data.attributes.ac.bonus
                   formula
                   calc
                   cover
                   flat
```

#### Add a Bonus to AC

E.g. An Item or Spell which adds to the Actor's current AC to something for the duration.

| Attribute Key | Change Mode | Effect Value |
| -------- | -------- | -------- |
| `data.attributes.ac.bonus`     | Add     | `+1`     |

#### Override the AC Calculation to a custom formula

E.g. An Item or Spell which sets the Actor's AC to something like `12 + Int` for the duration.

| Attribute Key | Change Mode | Effect Value |
| -------- | -------- | -------- |
| `data.attributes.ac.calc`     | Override     | `custom`     |
| `data.attributes.ac.formula`     | Custom     | `10 + @abilities.str.mod`     |
