# System Settings

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

?> World Setting: 🌐 | Client Setting: 👤

## Rest Variant 🌐

Allows the GM to select how long a [Rest](actors/player-characters.md#Resting) takes in game-time.

### Rest Variant Options

- Players Handbook (LR: 8 hours, SR: 1 hour)
- Gritty Realism (LR: 7 days, SR: 8 hours)
- Epic Heroism (LR: 1 hour, SR: 1 min)

## Diagonal Movement Rule 🌐

Controls how the Ruler measures diagonal spaces on a Square Grid.

### Diagonal Options

- PHB: Equidistant (5/5/5)
- DMG: Alternating (5/10/5)
- Euclidian (7.07 ft. Diagonal)

## Initiative Dexterity Tiebreaker 🌐

Appends an Actor's Dexterity ability score to the rolled initiative value. Useful to break ties.

## Apply Currency Weight 🌐

Makes carried currency count against the character's encumberance. Follows the PHB rules on pg. 143.

## Disable Experience Tracking 🌐

Removes the experience tracker from [Player Character](actors/player-characters.md sheets. Useful if you use Milestone leveling.

## Collapse Item Cards in Chat 👤

When [Items](items/items.md) are activated, the created chat card will have the description collapsed by default. This does not affect the presence of the buttons on the chat card.

## Allow [Polymorphing](guides/polymorph.md) 🌐

Instead of the GM having to polymorph an actor, this allows the player who owns that actor to do so on their own.

## Use Metric Weight Units 🌐

Replaces imperial units of weight with metric units:

- `lbs` -> `kgs`

Changes the Actor Encumberance calculations accordingly.

This does not convert any numerical weights of items or actors, only the unit of measurement displayed.
