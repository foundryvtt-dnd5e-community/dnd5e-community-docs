# Player Characters

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

### Class Configuration

[Class](items/class.md) Items store the data for how many levels of a particular class a given character has and what subclass they have chosen.

Adding a new Class item or increasing the "Level" of a class item present on a PC will do the following:

1. Prompt the user if they want the class features for the new levels added to the character.
    - Selecting "Apply" will pull the appropriate [Feature](items/feature.md) items from the [Class Feature Compendium](included-compendiums.md).
1. Increase the hit dice available to the character according to the new level and the hit die size of the class.

#### Primary Class

Configurable in the [Special Traits] dialog, the first class added to a character is its "Primary Class."

### Ability Save Proficiency

Checking the checkbox at the bottom of an ability value box will mark the actor as proficient with that ability's saving throws.

### Skill Proficiency

Checking the checkbox next to a skill will cycle that skill's proficiency between the following:

0. None (empty circle)
1. Proficient (half-moon circle)
2. Half-Proficient (single tick)
3. Expertise (Double Proficient) (double tick)

The lighter number in parenthesis to the far right of a skill's row is that skill's passive value.

### Item Proficiencies

The proficiency selectors for Weapons, Armor, and Tools directly drive the proficiency automations for those given types of items.

When these are configured, dragging a [Weapon](items/weapon.md), [Equipment](items/equipment.md), or [Tool](items/tool.md) item from the [Included Compendiums](included-compendiums.md) onto the Actor will set the proficiency of that item correctly.
This automatic setting of proficiency works by name matching

### Hit Dice

If you ever need to manually adjust the hit dice pool of a PC, or spend a hit die to heal outside of a short rest, this can be done with the Hit Dice adjustment dialog.

The Roll button follows the same logic as the Short Rest Dialog's dice roller.

### Armor Class

The Armor Class configuration dialog allows the user to select from a set of pre-made formula (or a custom formula) which calculate AC automatically, or to select 'Flat' and type in an override for the actor.

#### Calculation Types

With the exception of "Flat", all armor calculations take into account [Active Effects](active-effects.md) and Shield [Equipment](items/equipment.md) items.

##### Equipped Armor

Selecting the "Equipped Armor" selection will make the AC calculation take into account equipped [Equipment](items/equipment.md) Item which has an Armor type and Shield type.

:::warning
If there are multiple equipped armor/shield items, a warning will display and the AC calculated may be incorrect.
:::

##### Flat

The "Flat" type of Calculation will fully ignore any Equipment Items and [Active Effects](active-effects.md) related to armor, simply displaying the input value instead. Use this if you desire no automation in your game.

##### Natural Armor

The "Natural Armor" calculation will allow you to input an arbitrary number as the base AC, but still have shield items and active effects affect the final value.

### Movement

Configuring an actor's movement happens within the Movement Speed dialog. Each type of movement is individually represented.

The "Primary" movement displayed for a PC is always the "Walk" speed. Additional movement types are displayed beneath it.

### Senses

#### Token Vision

Not configured by the senses config.

---

- Senses
- Languages
- Immunities, Resistances, Vulnerabilities
- Resources, Setting Items up to consume Resources
- Explainer of Special Traits
- Adding Items, Equipping Items, Attuning to Items, Controlling the Quantity of an Item
- Adding Spells, Preparing Spells, Pact Magic
- Adding Spell Scrolls

## Usage

## Automations

### Resting

- Long/Short; New Day; Resource Recharging, Hit Points/Hit Dice

#### Long Rest

Taking a long rest from a PC Sheet will automatically replenish the following:

- All Missing Hitpoints
- All Missing Spell Slots
- Half of the Missing Hit Dice
- [Resources] marked as recharging on a long rest
- [Items](items/items.md) (this includes Features) which have Limited Uses configured to replenish on a long rest

Additionally, all Temporary Hit Points and any changes to the Maximum Hit Points are removed.

##### Is New Day?

Selecting the checkbox in the long rest dialog for "Is New Day?" will also recharge any items (including features) which are configured to replenish per Day as opposed to during a long rest.

#### Short Rest

Taking a short rest from an Actor Sheet will first allow the user to roll available hit dice to replenish HP.

Once satisfied with the hit dice rolled, clicking "Rest" will replenish the following:

- [Resources] marked as recharging during a short rest
- [Items](items/items.md) (including Features) which have Limited Uses configured to replenish on a short rest

### Death Saves

While a character has zero hitpoints, clicking the "Death Saves" text on the Attributes tab of the Actor Sheet will roll a death saving throw and automatically record the results.

Regaining Hit Points will reset the death save failures and successes to 0.

### Spell Slots

#### Class/Multiclass level based slot Calculation

#### Overrides

### Special Traits
