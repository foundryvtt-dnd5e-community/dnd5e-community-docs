# Actor Macro API

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

This page contains information about how to interact with `Actor5e` through the Javascript API that might be useful for [Macros](https://foundryvtt.com/article/macros/) related to [Actors](actors/actors.md).

## `Actor5e#rollAbilityTest(abilityId, options)`

Roll a generic ability test or saving throw.
Prompt the user for input on which variety of roll they want to do.

### Parameters

| Name      | Type             | Description                                    |
|-----------|------------------|------------------------------------------------|
| `abilityId` | `string` | The ability id (e.g. "str")                  |
| `options`    | `object`   | Options which configure how ability tests or saving throws are rolled         |

### Example Snippets

```javascript
actor.rollAbilityTest('str', { fastForward: true });
```
