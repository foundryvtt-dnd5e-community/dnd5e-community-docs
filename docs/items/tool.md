# Tool

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

<!-- tabs:start -->

### **Description**

[_description-physical](_description-physical.md ':include')

### **Details**

![Details panel of a Tool Item](./_images/tool/tool-details.png)

<!-- tabs:end -->

## Usage

- Rolling a tool check

## Automations
