# Loot

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

<!-- tabs:start -->
### **Description**

[_description-physical](_description-physical.md ':include')

<!-- tabs:end -->
## Usage

Loot items do not have a workflow attached, they simply print their description to chat.

## Automations

None
