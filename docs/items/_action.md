<!-- markdownlint-disable MD041 -->

![Blank item action section](./_images/common/item-action.png)

This section of the Item's Details tab allows the item's Attack, Saving Throw, Damage or Healing to be configured.

Once an "Action Type" is selected, the other fields in this section are available.

<details>
<summary>Available Action Types</summary>

| Option | Has Attack Roll     | Description |
|-----------|:------------------:|:----|
| Melee Weapon Attack | ✅                 | Any Melee Weapon |
| Ranged Weapon Attack | ✅                 | Any Ranged Weapon |
| Melee Spell Attack | ✅       | Any Melee Range Attack Spell |
| Ranged Spell Attack | ✅     | Any Ranged Attack Spell |
| Saving Throw |  | For items which only force a saving throw. |
| Healing |                  | No Attack Roll or Save, only Healing. |
| Ability Check |                  | Prompts for an ability check. |
| Utility |                  | No special functionality |
| Other |                  | Anything else |
</details>

##### Attack Rolls

If the item's Action Type is a kind of attack, when used, the item's chat card will allow an attack roll to be made.

The ability score used for that attack roll is controlled by the Ability Modifier field. Any extra bonuses (e.g. if this item is a magic item) to the attack roll can be entered in the Attack Roll Bonus field.

<details>
<summary>Default Attack Ability Modifiers</summary>

| Item Type | Other Property     | Default Ability Modifier                               |
|-----------|--------------------|--------------------------------------------------------|
| Spell     | --                 | Actor Spellcasting Modifier (Fallback: Int) |
| Tool      | --                 | Intelligence                                |
| Weapon    | Spell Attack       | Actor Spellcasting Modifier (Fallback: Int)    |
| Weapon    | Finesse Weapon     | the highest between Dexterity and Strength  |
| Weapon    | Ranged Weapon Type | Dexterity                                   |
| Weapon    | --                 | Strength                                    |
</details>

##### Damage Rolls

There are three types of Damage Roll possible from an item:

1. Default Damage
2. Versatile Damage
3. Other Formula (which is not strictly damage)

**Default Damage** for an Item is entered in several parts in the Damage Formula fields. These fields accept Foundry standard [Dice Roll formula](https://foundryvtt.com/article/dice/). Many damage parts can be added to the same item, which will be combined when the item's damage is rolled.

The **Versatile Damage** field only appears when there is at least one default damage formula. It determines if the item's resulting chat card contains a "Versatile" button, as well as what formula is used when that damage is rolled.

The **Other Formula** field is not strictly used for damage, but in cases where an item sometimes deals extra damage, it can be useful for that (e.g. Flame Tongue Swords).

##### Saving Throws

For Items which force a saving throw, this save can be configured with the "Saving Throw" inputs. The first of these fields controls what ability the prompted save should be. The last allows the DC to be determined automatically (see table below) or manually (flat save DC). These automatic Save DCs use the Actor who owns the item's statistics.

| DC Selection | Formula Used     |
|-----------|--------------------|
| Spellcasting     | Spellcasting DC               |
| Ability (Str, Con, etc)      | 8 + @prof + The Selected Ability Modifier                 |
| Flat    | Input Number (Formula not supported)       |

##### Chat Flavor

Any additional flavor text to be displayed when the item is used. An item does not need any damage, attack roll, or saving throw to display this flavor text on the chat card.
