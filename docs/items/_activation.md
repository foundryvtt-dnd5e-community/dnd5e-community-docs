<!-- markdownlint-disable MD041 -->

![Blank item activation section](./_images/common/item-activation.png)

##### Activation Cost

- cost
- type
- condition

The following fields only appear when the Item's Activation Cost has a type that is not blank (even "None" will make these appear).

##### Activation Condition

Displayed as flavor next to the selected Activation Cost Type.

##### Target

- value
- width (line type only)
- units
- type

##### Range

- value
- long
- units

##### Duration

- value
- units

##### Limited Uses

- value
- max
- per

##### Resource Consumption

- type
- target
- amount
