# Consumables

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

<!-- tabs:start -->
### **Description**

[_description-physical](_description-physical.md ':include')

### **Details**

#### Consumable Details

#### Consumable Usage

[_activation](_activation.md ':include')

##### Destroy on Empty

#### Consumable Activation

[_action](_action.md ':include')

### **Effects**

See: [Active Effects](active-effects.md)

<!-- tabs:end -->
## Usage

- Usual Workflow for Attack/Save + Damage

## Automations

- Destroy on Empty
