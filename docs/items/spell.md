# Spell

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

<!-- tabs:start -->

### **Description**

### **Details**

#### Spell Details

- Spell level
- Spell School
- Components
- Materials
- Preparation
  - Pact Magic
  - Innate
  - At Will

#### Spell Casting

[_activation](_activation.md ':include')

#### Spell Effects

[_action](_action.md ':include')

- Level Casting
- Cantrip Scaling

### **Effects**

See: [Active Effects](active-effects.md)

<!-- tabs:end -->

## Usage

- Measured Template Placement
- Usual Workflow for Attack/Save + Damage

## Automations

- Spell Slot consumption
- Cantrip Scaling
- Upcasting
- Effects from Spells apply to the spell owner?
