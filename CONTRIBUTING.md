# Contributing to these Docs

## Content

For now focus the content on strictly the Core dnd5e system with no modules installed. Once we have a stable baseline for that we will consider opening up to include module capabilities.

It is reasonable to include mention of modules in any Guides which are contributed.

## Contribution Process

All contributions to the `main` branch must go through [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html).

Each MR is expected to pass our linter before being merged. Information about running the Linter locally is in the [README](README.md#linting).

### Merge Request Focus

It is much easier to review, revise, and merge a focused MR than a sprawling one. Try to keep your MRs split into individual concerns to give reviewers an easier time, splitting up your contributions into multiple MRs if necessary/possible.

## Style Guide

### Interlinking

As often as is practical, link between pages of these documents, and to credible external sources of knowledge. The [Wikipedia Manual of Style](https://en.wikipedia.org/wiki/Wikipedia:Manual_of_Style/Linking) has excellent examples of "underlinking" and "overlinking".

Some guiding questions to ask:

- Will linking to something enable the reader to understand this content better?
- Has this (other) page been linked to recently within this document?

#### Examples of Credible External Sources

- foundryvtt.com Knowledge Base
- foundryvtt.wiki Community Wiki

### Linter

[Markdownlint](https://github.com/DavidAnson/markdownlint) is law. Its rules keep us orderly even when we do not agree with them. All MRs must meet the linter and a CI pipeline will enforce this.

### Relevant Version Annotation

A shield.io badge must be placed at the top of every page which details the "system version we know this document is up to date as of." This allows readers and contributors alike to know at a glance if the document they're looking at is out of date.

```
![Up to date as of <VERSION>](https://img.shields.io/badge/dnd5e-v<VERSION>-informational)
```

### Asset Management (Images, etc.)

#### Naming

Name the file descriptively in such a way that it makes it simple to find and replace in the future.

#### Placement

Assets for pages must be in a similarly named subdirectory of the nearest `_images` directory to the page using them.

#### Example

```
./docs/items
├── _images
│   └── backpack
│       └── backpack-details.png
└── backpack.md
```

The `backpack.md` file references a `backpack-details` image which is housed under the `_images/backpack` directory. In the future when the details tab of the backpack item type changes, replacing this image with a new one will be simple.

### Voice

The following apply to all pages outside of the "Guides" section. Guides are subject to a more relaxed set of guidelines and may exhibit more personal voice if the author chooses.

#### Be concise

Write as little as is necessary to get the concept across. Our goal is to be skimmable so that GMs and Players who are in a hurry can find what they are looking for quickly.

#### Be formal

Do not reference the user with "you" but instead choose wording such as "the user." Always reference "the user" or an Actor with the pronoun "they."

Emulate something which could be found in official documentation for a company's products, or something akin to large video gaming wikis (e.g. [The Official Minecraft Wiki](https://minecraft.fandom.com/wiki/Minecraft_Wiki)).

#### Strive to fit in

This documentation should read as though it were written in one voice, not many. Do your best to mimic the style of an existing page or section which applies to your content.

#### Own this collectively, not individually

Do not take it personally if someone's contributions rewrite portions of your own in order to either update them, correct them, or conform them to a shifting standard. We strive for a culture of working together to improve these docs.

## Page Anatomy

### Documents: Items and Actors

Each of the types of Item/Actor has a page, and each of these pages has three sections:

1. Configuration - Details what the various fields on the document do.
2. Usage - Details how to use the document via the UI.
3. Automations - Details any existing automations in the core system alone.
