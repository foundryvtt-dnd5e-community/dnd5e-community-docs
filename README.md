# dnd5e-community-docs

[Docs](https://foundryvtt-dnd5e-community.gitlab.io/dnd5e-community-docs/#/)

This is a community powered initiative to write down documentation for running D&D Fifth Edition in Foundry VTT with the [dnd5e system](https://gitlab.com/foundrynet/dnd5e). Its goal is to be the definitive guide to playing 5e with the vanilla system in written form so as to be searchable.

Powered by [Docsify](https://docsify.js.org), see those docs for more info about capabilities such as:

- [Markdown Helpers](https://docsify.js.org/#/helpers)
- [Embedded Files](https://docsify.js.org/#/embed-files)

## Contributing

Fork this project and open a Merge Request with the changes in documentation you would like merged in. Please pay attention to the style guidelines in [CONTRIBUTING](CONTRIBUTING.md).

## Local Development

> This repository is configured to use Gitpod. [Find out more.](https://gitlab.com/foundryvtt-dnd5e-community/dnd5e-community-docs/-/wikis/Using-Gitpod-to-Contribute)

Once cloned, run `npm install` to install all of the dependencies needed to use this repo's tooling.

### Local Docsify

```
npm run serve
```

Will run a local development version of the docsify site where you can see your changes before they are deployed.

### Linting

```
npm run lint
        lint:fix
```

This repository makes use of markdownlint rules and all Merge Requests are expected to pass that step. To ensure your local changes pass, make use of these commands. `lint:fix` will attempt to automatically fix as many problems as it can.

## License

Except as otherwise noted, the content of the documentation in this repository is licensed under the Creative Commons ShareAlike 4.0 License, and code samples are licensed under the Unlicense, see [LICENSE](/LICENSE) for more details.
